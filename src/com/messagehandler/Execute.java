package com.messagehandler;

import com.messagehandler.entity.Message;

import java.util.Random;
import java.util.concurrent.*;

public final class Execute {
    private final static int queueSize = 100;
    private static BlockingQueue<Message> queue;
    private static int resultCount = 0;
    private static long execTime = 0L;
    private static CountDownLatch producerLatch;
    private static CountDownLatch consumerLatch;

    private Execute() {
        queue = new LinkedBlockingQueue<>(queueSize);
        producerLatch = new CountDownLatch(queueSize);

        ExecutorService producerExecutor = Executors.newSingleThreadExecutor();
        producerExecutor.execute(new Producer());

        try {
            producerLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        producerExecutor.shutdown();
        printSourceQueue();

        ExecutorService threadManager = Executors.newFixedThreadPool(4);
        threadManager.execute(new ThreadManager());
        threadManager.shutdown();
    }

    public static void main(String[] args) {
        new Execute();
    }

    private final class Producer implements Runnable {
        @Override
        public void run() {
            Random rnd = new Random();
            while (queue.remainingCapacity()>0) {
                int groupId = rnd.nextInt(4) + 1;
                queue.add(new Message(queue.size() + 1, groupId));
                producerLatch.countDown();
            }
        }
    }

    private final class Consumer implements Runnable {
        private BlockingQueue<Message> localQueue;

        Consumer(final BlockingQueue<Message> localQueue) {
            this.localQueue = localQueue;
        }

        @Override
        public void run() {
            long startTime = System.currentTimeMillis();
            while (localQueue.size() > 0) {
                try {
                    handleMessage(localQueue.take());
                    consumerLatch.countDown();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            execTime += (System.currentTimeMillis() - startTime);
        }
    }

    private final class ThreadManager implements Runnable {
        private BlockingQueue<Message> queue1 = new LinkedBlockingQueue<>();
        private BlockingQueue<Message> queue2 = new LinkedBlockingQueue<>();
        private BlockingQueue<Message> queue3 = new LinkedBlockingQueue<>();
        private BlockingQueue<Message> queue4 = new LinkedBlockingQueue<>();

        private ExecutorService consumerExecutor = Executors.newFixedThreadPool(4);

        @Override
        public void run() {
            Message takenMsg;
            while (queue.size() != 0) {
                try {
                    takenMsg = queue.take();

                    switch (takenMsg.getGroupId()) {
                        case 1:
                            queue1.add(takenMsg);
                            break;
                        case 2:
                            queue2.add(takenMsg);
                            break;
                        case 3:
                            queue3.add(takenMsg);
                            break;
                        case 4:
                            queue4.add(takenMsg);
                            break;
                        default:
                            throw new IllegalArgumentException
                                    ("Incorrect groupId: " + takenMsg.getGroupId());
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            consumerLatch = new CountDownLatch(queueSize);
            consumerExecutor.execute(new Consumer(queue1));
            consumerExecutor.execute(new Consumer(queue2));
            consumerExecutor.execute(new Consumer(queue3));
            consumerExecutor.execute(new Consumer(queue4));

            try {
                consumerLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            consumerExecutor.shutdown();
            printResultCount();
        }
    }

    private static void handleMessage(Message msg) {
        System.out.print(msg.getItemId() + ":" + msg.getGroupId() + " ");
        resultCount++;
    }

    private static void printResultCount() {
        System.out.println();
        System.out.println("Handled items: " + resultCount);
        System.out.println("Execution time: " + execTime);
    }

    private static void printSourceQueue() {
        System.out.println("Queue: ");
        for (Message msg:queue) {
            System.out.print(msg.getItemId() + ":" + msg.getGroupId() + " ");
        }
        System.out.println();
        System.out.println("Queue size: " + queue.size());
        System.out.println("Handled queue:");
    }
}
