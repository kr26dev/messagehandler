package com.messagehandler.entity;


public final class Message {
    private final int itemId;
    private final int groupId;

    public Message(final int itemId, final int groupId) {
        this.itemId = itemId;
        this.groupId = groupId;
    }

    public int getItemId() {
        return itemId;
    }

    public int getGroupId() {
        return groupId;
    }
}
